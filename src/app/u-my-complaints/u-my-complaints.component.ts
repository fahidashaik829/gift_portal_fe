import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MycompService } from '../Shared/mycomp.service';
import { MyComplaints } from './mycomplaints.model';

@Component({
  selector: 'app-u-my-complaints',
  templateUrl: './u-my-complaints.component.html',
  styleUrls: ['./u-my-complaints.component.css']
})
export class UMyComplaintsComponent implements OnInit {
  formValue  !: FormGroup;
    mycomplaint : MyComplaints =new MyComplaints()
    complaints!:any
  constructor(private formbuilder:FormBuilder,private mycomp:MycompService ) { }

  ngOnInit(): void {
    this.formValue = this.formbuilder.group({
      cid: [''],
      oid: [''],
      message: [''],
      status:['']
      
    })
    this.getcomplaints();
  }
  getcomplaints() {
    this.mycomp.getcomplaints().subscribe(res => {
      console.log("data",res);
      this.complaints=res;
    });
  }

}
