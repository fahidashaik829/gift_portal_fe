import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UMyComplaintsComponent } from './u-my-complaints.component';

describe('UMyComplaintsComponent', () => {
  let component: UMyComplaintsComponent;
  let fixture: ComponentFixture<UMyComplaintsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UMyComplaintsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UMyComplaintsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
