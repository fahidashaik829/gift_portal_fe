import { Component, Input, OnInit, Output } from '@angular/core';
import { ApiService } from 'src/app/Shared/api.service';
import { MatTableDataSource } from '@angular/material/table';



@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  Uname !: any
  userdata !:any

  constructor(private api:ApiService) { }
  
  order: boolean = false;
  ngOnInit(): void {
    this.api.getUser().subscribe(res => {
      this.userdata = res;
    });
    }
  
    Search(){
      if(this.Uname == ""){
        this.ngOnInit();
      }else{
        this.userdata =this.userdata.filter((resp: { Uname: any; }) =>{
          return resp.Uname.toLocaleLowerCase().match(this.Uname.toLocaleLowerCase())
        })
       
      }
  }
  // key :string ='id';
  // reverse:boolean =false;
  // sort(key:any){
  //   this.key=key;
  //   this.reverse=!this.reverse;
  // }
}
