import { Component, OnInit, resolveForwardRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from '../../Shared/api.service';
import { UserModel } from './user-dashboard.modal';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css']
})
export class UserDashboardComponent implements OnInit {
  Uname !:any
  formValue  !: FormGroup;
  usermodelobj: UserModel = new UserModel();
  userdata !: any;
  showAdd !:boolean;
  loggedin :boolean = false;
  showUpdate !:boolean;
  constructor(private formbuilder: FormBuilder, private api: ApiService) { }
  display = "none";

  ngOnInit(): void {
    this.formValue = this.formbuilder.group({
      uid: [''],
      uname: [''],
      email: [''],
      password: ['']
    })
    if(localStorage.getItem('email')){
      this.loggedin= true;
      this.getAllUsers();
    }
    else{
      this.loggedin = false;
    }
  }
  onCloseHandled() {
    this.display = "none";
  }
  clickAddUser(){
    this.display = "block"
    this.formValue.reset();
    this.showAdd =true;
    this.showUpdate =false;
  }
  postUserDetails() {
    
    // this.usermodelobj.uid = this.formValue.value.UserID;
    this.usermodelobj.uname = this.formValue.value.uname;
    this.usermodelobj.email = this.formValue.value.email;
    this.usermodelobj.password = this.formValue.value.password;

    this.api.postUser(this.usermodelobj)
      .subscribe(res => {
        console.log(res);
        alert("User Added Successfully")
        this.formValue.reset();
        let ref =document.getElementById('cancel')
        ref?.click();
        this.getAllUsers();
      },
        err => {
          alert("Something went wrong");
        })
    }
    getAllUsers() {
      this.api.getUser().subscribe(res => {
        this.userdata = res;
      });
    }
    deleteUsercom(row :any){
      this.api.deleteUser(row.uid).subscribe(res=>{
        alert("User has been deleted");
        this.getAllUsers();
      })
    }
      onEdit(row :any){
        this.display = "block";
        this.showAdd = false;
        this.showUpdate =true;
        this.usermodelobj.uid = row.uid;
        this.formValue.controls['uname'].setValue(row.uname)
        this.formValue.controls['email'].setValue(row.email)
        this.formValue.controls['password'].setValue(row.password)
      }
      updateUserDetails(){
        this.usermodelobj.uname = this.formValue.value.uname;
    this.usermodelobj.email = this.formValue.value.email;
    this.usermodelobj.password = this.formValue.value.password;
    this.api.updateUser(this.usermodelobj,this.usermodelobj.uid).subscribe(res=>{
      alert("Updated Successfully");
        let ref =document.getElementById('cancel')
        ref?.click();
        this.formValue.reset();
        this.getAllUsers();
    })
  }Search(){
    if(this.Uname == ""){
      this.ngOnInit();
    }else{
      this.userdata =this.userdata.filter((resp: { Uname: any; }) =>{
        return resp.Uname.toLocaleLowerCase().match(this.Uname.toLocaleLowerCase())
      })
     
    }

    
  }
}

