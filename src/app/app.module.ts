import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';

import { RouterModule } from '@angular/router';
import { ApiService } from './Shared/api.service';
import { NgxPaginationModule} from 'ngx-pagination';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { MatFormFieldModule } from '@angular/material/form-field';


import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AdminHeaderComponent } from './admin-header/admin-header.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UMyComplaintsComponent } from './u-my-complaints/u-my-complaints.component';




@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    AdminHeaderComponent,
    UserProfileComponent,
    UMyComplaintsComponent,
    
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    MatPaginatorModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    BrowserAnimationsModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
