import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UprofService } from '../Shared/uprof.service';
import { UserProf } from './userprof.model';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
 formValue  !: FormGroup;
 display="none";
 userdata ! :any
 userprofobj : UserProf =new UserProf()
  constructor(private formbuilder: FormBuilder,private prof:UprofService ) { }

  ngOnInit(): void {
    this.formValue = this.formbuilder.group({
      uname: [''],
      email: [''],
      mobileno: [''],
      address: [''],
      city: [''],
      pincode :[''],
    })
    this.getUserprof();
}
 
 getUserprof() {
    let id=localStorage.getItem("uid")
      this.prof.getProf(id).subscribe(res => {
        this.userdata = res;
      });
    }

    
     updateUserprofile(){
      // this.userprofobj.uname = this.formValue.value.m;
        this.userprofobj.mobileno = this.formValue.value.mobileno;
    this.userprofobj.address = this.formValue.value.address;
    this.userprofobj.city = this.formValue.value.city;
      this.userprofobj.pincode = this.formValue.value.pincode;
    this.prof.updateProf(this.userprofobj,this.userprofobj.uid).subscribe(res=>{
      alert("Updated Successfully");
        let ref =document.getElementById('cancel')
        ref?.click();
        this.formValue.reset();
        this.getUserprof();
    })
    }

     onEditing(userdata :any){
        this.display = "block";
        
        this.userprofobj.uid = userdata.uid;
        this.formValue.controls['email'].setValue(userdata.email)
        this.formValue.controls['uname'].setValue(userdata.uname)
        this.formValue.controls['mobileno'].setValue(userdata.mobileno)
        this.formValue.controls['address'].setValue(userdata.address)
        this.formValue.controls['city'].setValue(userdata.city)
        this.formValue.controls['pincode'].setValue(userdata.pincode)
      }
      onCloseHandled() {
    this.display = "none";
  }
}
