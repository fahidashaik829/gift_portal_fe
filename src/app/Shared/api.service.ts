import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import  {map} from 'rxjs/Operators';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http:HttpClient) { }

  postUser(data :any){
    return this.http.post<any>("http://325c-49-205-212-89.ngrok.io/admin/addusers/",data)
    .pipe(map((res:any)=>{
      return res;
    }))
  }
    getUser(){
      return this.http.get<any>("http://19d8-49-205-212-89.ngrok.io/admin/showAllUsers")
      .pipe(map((res:any)=>{
        return res;
      }))
    }
      updateUser(data: any,id: any){
        return this.http.put("http://325c-49-205-212-89.ngrok.io/admin/users/"+id+"/",data)
        .pipe(map((res:any)=>{
          return res;
        }))
      }
      deleteUser(id: number){
        return this.http.delete<any>("http://325c-49-205-212-89.ngrok.io/admin/users/"+id)
        .pipe(map((res:any)=>{
          return res;
        }))
      }
        
  
}

