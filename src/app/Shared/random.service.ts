import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import  {map} from 'rxjs/Operators';
@Injectable({
  providedIn: 'root'
})
export class RandomService {
  private url :string='http://19d8-49-205-212-89.ngrok.io/'
  constructor(private http: HttpClient) { }
  getcomplaints(){
    return this.http.get<any>(`${this.url}admin/showAllComplaints`)
    .pipe(map((res:any)=>{
      console.log("servicedata",res);
      return res;
    }))
  }
  setstatus(i:any,id:any){
    return this.http.put<any>(`${this.url}admin/complaints/${id}/`,i)
  .pipe(map((res:any)=>{
    return res;
  }))
}
  }
