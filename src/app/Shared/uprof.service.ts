import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import  {map} from 'rxjs/Operators';

@Injectable({
  providedIn: 'root'
})
export class UprofService {
  private url = 'http://6fb6-49-205-213-213.ngrok.io'
  constructor(private http:HttpClient) { }

  getProf(id:any){
      return this.http.get<any>(`${this.url}/User/profile/${id}`)
      .pipe(map((res:any)=>{
        return res;
      }))
    }

updateProf(data: any,id: any){
        return this.http.put(`${this.url}/User/profile/${id}`,data)
        .pipe(map((res:any)=>{
          return res;
        }))
      }
}
