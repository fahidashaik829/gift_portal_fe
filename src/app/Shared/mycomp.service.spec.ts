import { TestBed } from '@angular/core/testing';

import { MycompService } from './mycomp.service';

describe('MycompService', () => {
  let service: MycompService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MycompService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
