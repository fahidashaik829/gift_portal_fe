import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import  {map} from 'rxjs/Operators';
@Injectable({
  providedIn: 'root'
})
export class AdminlogService {
    private url='http://5325-49-205-212-89.ngrok.io/';
  constructor(private http:HttpClient) { }
  loginAdmin(data :any){
    return this.http.post<any>(`${this.url}admin/`,data)
    .pipe(map((res:any)=>{
      return res;
    }))
  }
  loginUser(data :any){
    return this.http.post<any>(`${this.url}User/`,data)
    .pipe(map((res:any)=>{
      return res;
    }))
  }
}
