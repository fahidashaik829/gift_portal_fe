import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import  {map} from 'rxjs/Operators';

@Injectable({
  providedIn: 'root'
})
export class MycompService {
  private url:String='http://1bff-49-205-213-213.ngrok.io'
  constructor(private http:HttpClient) { }

  getcomplaints(){
    let id = localStorage.getItem('uid')
    return this.http.get<any>(`${this.url}/User/complaints/${id}`)
    .pipe(map((res:any)=>{
      console.log("servicedata",res);
      return res;
    }))
  }
}
