import { TestBed } from '@angular/core/testing';

import { UprofService } from './uprof.service';

describe('UprofService', () => {
  let service: UprofService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UprofService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
