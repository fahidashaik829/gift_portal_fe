import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComplaintsComponent } from '../complaints/complaints.component';
import { OrdersComponent } from '../orders/orders.component';
import { UserDashboardComponent } from '../user/user-dashboard/user-dashboard.component';

const routes: Routes = [
  {path:'admin/orders', component:OrdersComponent},
  {path:'admin/complaints', component:ComplaintsComponent},
 {path: 'admin/users', component:UserDashboardComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingRoutingModule { }
