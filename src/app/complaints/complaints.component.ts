import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { RandomService } from '../Shared/random.service';
import { ComplaintModel } from './complaints.modal';

@Component({
  selector: 'app-complaints',
  templateUrl: './complaints.component.html',
  styleUrls: ['./complaints.component.css']
})
export class ComplaintsComponent implements OnInit {
  compModel :ComplaintModel =new ComplaintModel()
  formValue  !: FormGroup;
  display="none";
  Uname !: any
  complaintsdata !:any ; 
  constructor(private random:RandomService,private formbuilder: FormBuilder) { }

  ngOnInit(): void {
    this.formValue = this.formbuilder.group({
      cid: [''],
      uid: [''],
      oid: [''],
      message: [''],
      status:['']
      
    })
    this.getcomplaints();
  }
  onCloseHandled(){
    this.display ="none"
  }
  OnEdit(complaint:any){
    this.display="block"
    this.compModel.cid= complaint.cid;
    this.formValue.controls['status'].setValue(complaint.status)
  }

  OnUpdate(){
    this.display="none";
    this.compModel.status=this.formValue.value.status;
    this.random.setstatus(this.compModel,this.compModel.cid).subscribe(resps=>{
      this.formValue.reset();
      this.getcomplaints();
    })
  }

  getcomplaints() {
    this.random.getcomplaints().subscribe(res => {
      console.log("Cdata",res);
      this.complaintsdata = res;
    });
  }

  Search(){
    if(this.Uname == ""){
      this.ngOnInit();
    }else{
      this.complaintsdata =this.complaintsdata.filter((resp: { Uname: any; }) =>{
        return resp.Uname.toLocaleLowerCase().match(this.Uname.toLocaleLowerCase())
      })
     
    }

}
}