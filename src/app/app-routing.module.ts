import { componentFactoryName } from '@angular/compiler';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminHeaderComponent } from './admin-header/admin-header.component';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import { ComplaintsComponent } from './complaints/complaints.component';
import { ComplaintModel } from './complaints/complaints.modal';
import { OrdersComponent } from './orders/orders.component';
import { UserDashboardComponent } from './user/user-dashboard/user-dashboard.component';



const routes: Routes = [
  // { path: '', redirectTo: 'tutorials', pathMatch: 'full' },
  {path:'', component:AdminLoginComponent},
  {path:'admin', component:AdminHeaderComponent},
  {path:'admin/orders', component:OrdersComponent},
  {path:'admin/complaints', component:ComplaintsComponent},
 {path: 'admin/users', component:UserDashboardComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents =[AdminLoginComponent,ComplaintsComponent,OrdersComponent,UserDashboardComponent]