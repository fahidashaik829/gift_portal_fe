import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { OrdService } from '../Shared/ord.service';
import { MyOrdersModel } from './ordersModel';


@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  Uname !:any
  display = "none";
  formValue !: FormGroup;
  ordersModelObj: MyOrdersModel = new MyOrdersModel();
  ordersdata !: any;
  constructor(private formbuilder: FormBuilder, private ord: OrdService) { }
  onCloseHandled() {

    this.display = "none";

  }
  onOpenhandled() {
    this.display = "block";
  }

  ngOnInit(): void {
    this.formValue = this.formbuilder.group({
      oid: [''],
      uid: [''],
      productname: [''],
      rxname: [''],
      rxaddress: [''],
      rxcity: [''],
      rxpincode: [''],
      rxmobilenum: [''],
      ostatus: [''],
      trackstatus:[''],
      updates:[''],
      amount:['']
    })
    this.getOrders();

  }
 
  getOrders() {

    this.ord.getOrders().subscribe(res => {

      this.ordersdata = res;

    });

  }
  OnUpdate(){
    this.display="none";
    this.ordersModelObj.ostatus=this.formValue.value.ostatus;
    this.ordersModelObj.trackstatus=this.formValue.value.trackstatus;
    
    this.ord.setstatus(this.ordersModelObj,this.ordersModelObj.oid).subscribe(resps=>{
      this.formValue.reset();
      this.getOrders();
    })
  }
    OnEdit(order:any){
      this.display="block"
      this.ordersModelObj.oid= order.oid;
      this.formValue.controls['ostatus'].setValue(order.ostatus)
      this.formValue.controls['trackstatus'].setValue(order.trackstatus)
    }

}