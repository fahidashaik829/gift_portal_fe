import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AdminlogService } from '../Shared/adminlog.service';
import { AdminModel } from './admin.modal';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {
  formValue  !: FormGroup;
  adminModel: AdminModel =new AdminModel()
 

  constructor(private admin:AdminlogService,private formbuilder:FormBuilder,private router:Router) { }

  ngOnInit(): void {
    this.formValue = this.formbuilder.group({
      email: [''],
      password: ['']

    })
  }
  loginAdmin() {
    // e.preventDefault();
    // console.log(e);
    // var email =e.target.elements[0].value;
    // var password=e.target.elements[1].value;
    this.adminModel.email = this.formValue.value.email;
    this.adminModel.password = this.formValue.value.password;
    console.log(this.adminModel);
    // localStorage.setItem("email",this.formValue.value.email);
    this.admin.loginAdmin(this.adminModel)
    .subscribe(res => {
      console.log(res);
    
      if(res==true){
        this.router.navigate(['admin/users']);
        localStorage.setItem("email",this.formValue.value.email);
      }
      else{
        alert(" Wrong Credentials! Tryagain!")
      }
    })
    // return false;
  }
  loginUser(){
    this.adminModel.email = this.formValue.value.email;
    this.adminModel.password = this.formValue.value.password;
    console.log(this.adminModel);
    // localStorage.setItem("email",this.formValue.value.email);
    this.admin.loginUser(this.adminModel)
    .subscribe(res => {
      console.log(res);
    
      if(res.uid){
        this.router.navigate(['user/home']);
        localStorage.setItem("uid",res.uid);
      }
      else{
        alert(" Wrong Credentials! Tryagain!")
      }
    })

  }
}
